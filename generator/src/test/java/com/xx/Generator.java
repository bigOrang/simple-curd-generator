package com.xx;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.xx.lib.EnhanceFreemarkerTemplateEngine;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * https://baomidou.com/pages/779a6e/ 新
 * <p>
 * https://baomidou.com/pages/d357af/ 旧（参数详解）
 * <p>
 * 自定义模板有哪些可用参数？Github (opens new window)AbstractTemplateEngine 类中方法 getObjectMap 返回 objectMap 的所有值都可用。
 */
public class Generator {

    // test 123 456
    @Test
    public void generatorTest() {

        // 定位
        String finalProjectPath = System.getProperty("user.dir");
        System.out.println(finalProjectPath);

        // 执行
        FastAutoGenerator
                .create("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8", "root", "root")
                .globalConfig(builder -> {
                    // 设置作者
                    builder.author("kanan")
                            // 开启 swagger 模式
                            .fileOverride().enableSwagger()
                            //禁止打开输出目录
                            .disableOpenDir()
                            // 指定输出目录
                            .outputDir(finalProjectPath + "/src/main/java");
                })
                .packageConfig(builder -> {
                    // 设置父包名
                    builder.parent("com.xx")
                            // 设置mapperXml生成路径
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, finalProjectPath + "/src/main/resources"));
                })
                .strategyConfig(builder -> {
                    // 需要被解析的表名
                    builder.addInclude("auth_permission");
                    builder.entityBuilder().enableLombok().enableTableFieldAnnotation();
                    builder.controllerBuilder().enableRestStyle();
                })
                .injectionConfig(consumer -> {
                    Map<String, String> customFile = new HashMap<>();
                    // DTO 下面的key会作为类名后缀，进而生成新类
                    customFile.put("DTO.java", "templates/other/dto.java.ftl");
                    customFile.put("VO.java", "templates/other/vo.java.ftl");
                    customFile.put("Convertor.java", "templates/other/convertor.java.ftl");
                    consumer.customFile(customFile);
                })
                // EnhanceFreemarkerTemplateEngine 里主要重写对自定义类的处理 如vo dto convert等
                .templateEngine(new EnhanceFreemarkerTemplateEngine()).execute();
    }
}
