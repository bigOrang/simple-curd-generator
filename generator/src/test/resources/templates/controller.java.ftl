package ${package.Controller};


import com.xx.entity.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import com.xx.service.*;
import com.xx.other.*;
import javax.validation.constraints.NotEmpty;
import com.xx.BizResult;
import com.xx.bean.entity.PageRequest;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    /**
     * service
     */
    @Resource
    private ${table.serviceName} service;


    /**
     * 保存
     *
     * @param dto 参数
     * @return 保存结果
     */
    @GetMapping
    public BizResult<Boolean> saveOne(@Validated AuthPermissionDTO dto) {
        return BizResult.success(this.service.saveOne(dto));
    }

    /**
     * 根据主键查询VO
     *
     * @param pk 主键
     * @return VO
     */
    @GetMapping
    public BizResult<${entity}VO> getByPk(@Validated @NotEmpty String pk) {
        return BizResult.success(this.service.getByPk(pk));
    }

    /**
     * 根据主键删除
     *
     * @param pk 主键
     * @return 删除结果
     */
    @DeleteMapping("/{pk}")
    public BizResult<Object> deleteByPk(@Validated @NotEmpty @PathVariable("pk") String pk) {
        return BizResult.success(this.service.deleteByPk(pk));
    }


    /**
     * 分页查询
     *
     * @return BizResult
     */
    @GetMapping("/page")
    public BizResult<IPage<${entity}VO>> page(${entity}DTO param, PageRequest request) {
        IPage<${entity}> page = new Page<>(request.getPageNum(), request.getPageSize());
        IPage<${entity}VO> list = this.service.selectPageByDto(page, param);
        return BizResult.success(list);
    }
}
</#if>
