package com.xx.bean.convertor;

public interface AbstractConvertor<VO, DTO, ENTITY> {

    VO toVO(ENTITY entity);

    ENTITY toEntity(DTO dto);

}
